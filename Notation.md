## Chapter 02 - Getting Started

### Useful commands

- `npx spago init`: Create a new project in an empty directory
- `npx spago build`: Compile module to CommonJS Module
- `npx spago test`: Run test suite
- `npx spago run`: Compile and run the `Main` module
- `npx spago bunde-app`: Compile to JS
- `npx spago install <ModuleName>`: Install a module
- `npx spago repl`: Start interactive mode of PureScript (PSCi)

## Chapter 03 - Functions and Records

### Simple Types

PureScript defines three built-in types which correspond to JS's primitive types: numbers, strings and booleans.

#### Example: Simple Types

        $ npx spago repl
        > :type 1.0
        Number

        > :type "test"
        String

        > :type true
        Boolean
        ---

        > :type 1
        Int

        > :type 'a'
        Char

        > :type [1, 2, 3]
        Array Int

        > :type [true, false]
        Array Boolean

        > :type [1, false]
        Could not match type Int with type Boolean

### Arrays

Arrays correspond to JS's arrays, **but all elements must have the same type!**

### Records

Records correspond to JS's objects, and record literals have the same syntax as JS's object literals

        > author = { name: "Phil", interests: ["Functional Programming", "JavaScript"] }

        > :type author
        { name :: String
        , interests :: Array String
        }

This types indicates that the specified object has two _fields_, a `name` field which has type `String`, and an `interests` field, which has type `Array String`, i.e an array of `String`s.

### Functions

Functions can be defined at the top-level of a file by specifying arguments before the equals sign. Alternatively, functions can be defined by using a backlash character followed by a space-delimited list of argument names.
To enter a multi-line declaration in PSCi, we can enter "paste mode" by using the `:paste` command. Terminate this mode with _Control-D_.

        add: Int -> Int -> Int
        add x y = x + y

        > :paste
        ... add :: Int -> Int -> Int
        ... add = \x y -> x + y
        ... ^D

### Quantified Types

`flip` function has the following type: > :type flip
forall a b c. (a -> b -> c) -> b -> a -> c

The keyword `forall` here indicates that `flip` has a _universally quantified type_. That means, that we can substitute any types for `a`, `b` and `c` and `flip` will work with thos types.

If we set `a` to be `Int`, `b` and `c` to be a `String` we would _specialize the type_ of `flip` to:

> (Int -> String -> String) -> String -> Int -> String

### Type Constructor

`List` is _type constructor_.

Values do not have a type `List` directly, but rather `List a` for some type `a`.

`List` takes a _type argument_ `a` and _constructs_ a new type `List a`.

#### Example

`List Entry` is the type constructor `List` _applied_ to the type `Entry`. It represents a list of entries.

### Kind Error

Just like values are distinguished by their _types_, types are destinguished by their _kinds_.

Ill-typed values result in _type errors_.
Ill-kinded types result in _kind errors_

### Type Kind

There is a special kind called `Type` whie represents the kind of all types which have values, like `Number` and `String`.

#### Example

`Type -> Type` represents a function from types to types and there is a difference if a type constructor has the kind `Type -> Typ` or `Type`.

        > :kind Number
        Type

        > import Data.List
        :kind List
        Type -> Type

        :kind List String
        Type

**TO find out the kind of a type, use `:kind` command in PSCi.**

### Function Definition and Function Declaration

A function definition begins with the name of the function, followed by a list of argument names. The result of the function is specified after the equals sign.

This is a function declaration:

        showEntry :: Entry -> String

- `showEntry` is the name of the function.
- `::` is the type declaration.
- `Entry -> String` is the type of the function.
  Everything together is called a **type signature** and says that `showEntry` is a function, which takes an `Entry` as an argument and returns a `String`.

#### Example

This type signature says, that the function takes a value of some type `a`, and a list of elements of type `a`, and returns a new list with entries of the same type.

        forall a. a -> List a -> List a

#### Example: Concatenation

The function concatenates the three fields of the `Entry` record into a single string, using the `showAddress` function to turn the record inside the `address` field into a `String`

        showEntry entry = entry.lastName <> ", " <>
                  entry.firstName <> ": " <>
                  showAddress entry.address

`showAddress` is defined as:

        showAddress :: Address -> String
        showAddress addr = addr.street <> ", " <>
                   addr.city <> ", " <>
                   addr.state

This type signature says that insertEntry takes an Entry as its first argument, and an AddressBook as a second argument, and returns a new AddressBook:

        insertEntry :: Entry -> AddressBook -> AddressBook

### Immutable data structure

We don't modify the existing AddressBook directly. Instead, we return a new AddressBook which contains the same data. As such, AddressBook is an example of an _immutable data structure_. This is an important idea in PureScript - mutation is a side-effect of code, and inhibits our ability to reason effectively about its behavior, so we prefer pure functions and immutable data where possible.

### Curried Functions

**Functions in PureScript take exactly one argument**. While it looks like the insertEntry function takes two arguments, it is in fact an example of a _curried function_.

The `->` operator in the type of `insertEntry` associates to the right, which means that the compiler parses the type as

        Entry -> (AddressBook -> AddressBook)

That is, `insertEntry` **is a function which returns a function!** It takes a single argument, an `Entry`, and returns a new function, which in turn takes a single `AddressBook` argument and returns a new `AddressBook`.

### Eta conversion

Now consider the definition of `insertEntry`:

        insertEntry :: Entry -> AddressBook -> AddressBook
        insertEntry entry book = Cons entry book

If two functions have the same result for every input, then they are the same function! So we can remove the argument `book` from both sides!

        insertEntry :: Entry -> AddressBook -> AddressBook
        insertEntry entry = Cons entry

But now, by the same argument, we can remove entry from both sides:

        insertEntry :: Entry -> AddressBook -> AddressBook
        insertEntry = Cons

The process is called **eta conversion** and can be used to rewrite functions in _point-free form_, which means functions defined without reference to their argument.

#### Example Filter Function

        $ spago repl

        > import Data.List
        > :type filter

        forall a. (a -> Boolean) -> List a -> List a

        > :type head

        forall a. List a -> Maybe a

`filter` is a curried function of two arguments. It's first argument is a function, which takes an element of the list an returns a `Boolean` vlaue as a result. Its seond argument is a list of elements, and then return value is another list.

        $ spago repl

        > import Data.List

        > :type head

        forall a. List a -> Maybe a

`head` takes a list as its argument, and returns a type `Maybe`. `Maybe` represents an optional value of type `a` and provides a type-safe alternative to using `null` to indicate a missing value in languages like JS.

#### Example: Find entry

        findEntry firstName lastName book = head $ filter filterEntry book
         where
          filterEntry :: Entry -> Boolean
          filterEntry entry = entry.firstName == firstName && entry.lastName == lastName

`findEntry` brings three names into scope: `firstName`, and `lastName`, both representing strings, and `book` an `Addressbook`.

The right hand side of the definition combines the `filter` and `head` functions: first, the list of entries is filtered, and the `head` function is applied to the result.

The predicate function `filterEntry` is defined as an auxiliary declaration inside a `where` clause. This way, the `filterEntry` function is available inside the definition of our function, but not outside it. Also, it can depend on the arguments to the enclosing function, which is essential here because `filterEntry` uses the `firstName` and `lastName` arguments to filter the specified `Entry`.

### Infix symbol \$ stands for apply

        apply :: forall a b. (a -> b) -> a -> b
        apply f x = f x

        infixr 0 apply as $

So `apply` takes a function and a value and applies the function to the value. The `infixr` keyword is used to define `($)` as an alias for `apply`.

In the code for `findEntry` above, we used a different form of function application: the `head` function was applied to the expression `filter filterEntry` book by using the infix `$` symbol.

This is equivalent to the usual application `head (filter filterEntry book)`

### Function Composition

`<<<` and `>>>` are composition operators. `<<<` is the "backwards composition" operator, and `>>>` the "forwards composition" operator.

## Chapter 04 - Recursion, Maps And Folds

#### Example for Recursion: Factorial

        fact :: Int -> Int
        fact 0 = 1
        fact n = n * fact (n - 1)

Computes the factorial for an input integer `n`.

#### Example for Recursion: Fibonnacci:

        fib :: Int -> Int
        fib 0 = 1
        fib 1 = 1
        fib n = fib (n - 1) + fib (n - 2)

#### Example for Recursion: Length of Array

        import Prelude

        import Data.Array (null, tail)
        import Data.Maybe (fromMaybe)

        length :: forall a. Array a -> Int
        length arr =
        if null arr
        then 0
        else 1 + (length $ fromMaybe [] $ tail arr)

In this function, we use an `if .. then .. else` expression to branch based on the emptiness of the array. The `null` function returns `true` on an empty array. Empty arrays have length zero, and a non-empty array has a length that is one more than the length of its tail.

The `tail` function returns a `Maybe` wrapping the given array without its first element. If the array is empty (i.e. it doesn't has a tail) `Nothing` is returned. The `fromMaybe` function **takes a default value and a Maybe value**. If the latter is `Nothing` it returns the default, in the other case it returns the value wrapped by `Just`.

### Maps

#### Exmaple map

        $ spago repl

        > import Prelude
        > map (\n -> n + 1) [1, 2, 3, 4, 5]
        [2, 3, 4, 5, 6]

Notice how map is used - we provide a function which should be "mapped over" the array in the first argument, and the array itself in its second.

### Infix Operators - Backticks

The map function can also be written between the mapping function and the array, by **wrapping the function name in backticks**:

        > (\n -> n + 1) `map` [1, 2, 3, 4, 5]
        [2, 3, 4, 5, 6]

This syntax is called **infix function application**, and any function can be made infix in this way. It is usually most appropriate for functions with two arguments.

#### `<$>` Operator

There is an operator which is equivalent to the map function when used with arrays, called **<\$>**. This operator can be used infix like any other binary operator:

#### Example: Using `<$>` operator

Apply the show function to each element in the array and return an
array with the result of each application.

        > show <$> [1, 2, 3, 4, 5]

        ["1","2","3","4","5"]

Infix function names are defined as aliases for existing function names. For example, the Data.Array module defines an infix operator `(..)` as a **synonym for the range function**.

### Filtering Arrays

The `Data.Array` module provides another function `filter`, which is commonly used together with `map`. It provides the ability to create a new array from an existing array, keeping only those elements which match a predicate function.

For example, suppose we wanted to compute an array of all numbers between 1 and 10 which were even. We could do so as follows:

        > import Data.Array

        > filter (\n -> n `mod` 2 == 0) (1 .. 10)
        [2,4,6,8,10]

### Flattening Arrays

Another standard function on arrays is the concat function, defined in Data.Array. concat flattens an array of arrays into a single array: > import Data.Array

        > :type concat
        forall a. Array (Array a) -> Array a

        > concat [[1, 2, 3], [4, 5], [6]]
        [1, 2, 3, 4, 5, 6]

#### Example concatMap:

There is a related function called `concatMap` which is like a combination of the `concat` and `map` functions. Where `map` takes a function from values to values (possibly of a different type), `concatMap` takes a function from values to arrays of values.

        > import Data.Array

        > :type concatMap
        forall a b. (a -> Array b) -> Array a -> Array b

        > concatMap (\n -> [n, n * n]) (1 .. 5)
        [1,1,2,4,3,9,4,16,5,25]

Here, we call `concatMap` with the function `\n -> [n, n * n]` which sends an integer to the array of two elements consisting of that integer and its square. The result is an `array` of ten integers: the integers from 1 to 5 along with their squares.

Note how concatMap 1 its results. It calls the provided function once for each element of the original array, generating an array for each. Finally, it collapses all of those arrays into a single array, which is its result.

`map`, `filter` and `concatMap` form the basis for a whole range of functions over arrays called **"array comprehensions"**.

### Array Comprehensions

#### Example: This will create a list of two-element lists.

        > :paste
        … pairs'' n =
        …   concatMap (\i ->
        …     map (\j -> [i, j]) (i .. n)
        …   ) (1 .. n)
        … ^D
        > pairs'' 3
        [[1,1],[1,2],[1,3],[2,2],[2,3],[3,3]]

Explanation:

- 1. `(1 .. n)`: Create the range 1 to n.
- 2. `concatMap (\i -> ...) (1 .. n)`: Use `concatMap` and iterate over each element of the range created in (1). Those values are accessible via the variable `i`.
- 3. `map (\j -> [i, j]) (i .. n)`: This also creates a new range from `i` to `n`. The values are accessible via variable `j`. We use the `map` function to create a new list containing the value of the outer variable `i` and the current value of `j`.

We can use the function now to find the potential factors for a given number `n` with

        > import Data.Foldable

        > factors n = filter (\pair -> product pair == n) (pairs'' n)

        > factors 10
        [[1,10],[2,5]]

### Do Notation

        factors :: Int -> Array (Array Int)
        factors n = filter (\xs -> product xs == n) $ do
        i <- 1 .. n
        j <- i .. n
        pure [i, j]

The keyword `do` intorduces a block of code which uses do notation. The block consists of expressions of a few types:

- Expressions which bind elements of an array to a name. These are indicated with the backwards facing arrow `<-` with a name on the left, and an expression on the right whose type is an array. See `i <- 1 .. n` above.
- Expressions which do not bind elements of the array to names. The `do` _result_ is an example of this kind of expression and is illustrated in the last line, `pure [i, j]`.
- Expressions which give names to expressions, using the `let` keyword.

The `pure` function constructs a singleton array in this case but instead of `pure [i, j]` we could have written `[[i, j]]` as well.

### Guards

One further change we can make to the `factors` function is to move the filter inside the array comprehension. This is possible using the `guard` function from the `Control.MonadZero module` (from the control package):

        import Control.MonadZero (guard)

        factorsV3 :: Int -> Array (Array Int)
        factorsV3 n = do
        i <- 1 .. n
        j <- i .. n
        guard $ i * j == n
        pure [i, j]

Guard has the following signature: > import Control.MonadZero

        > :type guard
        forall m. MonadZero m => Boolean -> m Unit

which is `Boolean -> Array Unit` in the case of the `factors` function.

#### Example: Guard function applied to arrays

If `guard` is passed an expression which evaluates to `true`, then it returns an array with a single element. If the expression evaluates to `false`, then its result is empty.

        > import Data.Array

        > length $ guard true
        1

        > length $ guard false
        0

This means that if the guard fails, then the current brnach of array comprehension will terminate early **with no results**. This is equivalent to using `filter` on the intermediate array.

### Folds

        > import Data.Foldable

        > :type foldl
        forall a b f. Foldable f => (b -> a -> b) -> b -> f a -> b

        > :type foldr
        forall a b f. Foldable f => (a -> b -> b) -> b -> f a -> b

These types are actually more general than we are interested in right now. For the purposes of this chapter, we can assume that PSCi had given the following (more specific) answer:

        > :type foldl
        forall a b. (b -> a -> b) -> b -> Array a -> b

        > :type foldr
        forall a b. (a -> b -> b) -> b -> Array a -> b

In both of these cases, the type `a` corresponds to the type of elements of our array. The type `b` **can be thought of as the type of an "accumulator"**, which will accumulate a result as we traverse the array.

#### Explanation:

- `(b -> a -> b)`: Is a function that "adds" the next value of `a` to the accumulator `b` and returns a new accumulator `b`. (add can be any function, it must not be add.)
- `-> b -> `: Describes the initial value of the accumulator.
- `Array a` : Describes an array of type `a`, over which we fold.
- `-> b`: The last part describes, that a new accumulator value will be returned.

#### Example: Sum elements in an array:

Add the elements of the array [1,2,3,4,5] to the accumulator with the staring value of zero:

        > import Data.Foldable (foldl)

        > foldl (+) 0 (1 .. 5)
        15

> An example of folding would be the `reduce` function in JS.

- `foldl`: folds the array "from the left"
- `foldr`: folds the array "from the right"

#### Example of folding from the left and from the right

**Remember:** String concatenation uses the diamond operator `<>` instead of the plus operator.
**show: ** The Show type class represents those types which can be converted into a human-readable String representation.

        > foldl (\acc n -> acc <> show n) "" [1,2,3,4,5]
        "12345"

        > foldr (\n acc -> acc <> show n) "" [1,2,3,4,5]
        "54321"

### Tail Recursion

The key observation which enables tail recursion optimization is the following: a recursive call in `tail position` to a function can be replaced with a `jump`.
A call is in `tail position` when it is the last call made before a function returns.

#### Example: Not in tail position / Overflow

        > f 0 = 0
        > f n = 1 + f (n - 1)

        > f 10
        10

        > f 100000
        RangeError: Maximum call stack size exceeded

#### Example: in tail position

        factTailRec :: Int -> Int -> Int
        factTailRec 0 acc = acc
        factTailRec n acc = factTailRec (n - 1) (acc * n)

Notice that the recursive call to factTailRec is the last thing that happens in this function - it is in tail position.

The line is evaluated from right to left. That is whay factTailRec is in tail position, whereas `1 + f(n -1)` the addition `1 +` is in tail position, and not the function.
This causes the overflow.

### Accumulators

One common way to turn a function which is not tail recursive into a tail recursive function is to use an **accumulator parameter**. n accumulator parameter is an additional parameter which is added to a function which _accumulates_ a return value, as opposed to using the return value to accumulate the result.

For example, consider again the length function presented in the beginning of the chapter:

        length :: forall a. Array a -> Int
        length arr =
        if null arr
        then 0
        else 1 + (length $ fromMaybe [] $ tail arr)

This implementation is not tail recursive, so the generated JavaScript will cause a stack overflow. However, we can make it tail recursive, by introducing a second function argument to accumulate the result instead:

        lengthTailRec :: forall a. Array a -> Int
        lengthTailRec arr = length' arr 0
        where
        length' :: Array a -> Int -> Int
        length' arr' acc =
        if null arr'
                then acc
                else length' (fromMaybe [] $ tail arr') (acc + 1)

In this case, we delegate to the helper function `length'`, which is tail recursive - its only recursive call is in the last case, and is in `tail position`. This means that the generated code will be a while loop, and will not blow the stack for large inputs.

To understand the implementation of `lengthTailRec`, note that the helper function `length'` essentially uses the accumulator parameter to maintain an additional piece of state - the partial result. It starts out at 0, and grows by adding 1 for every element in the input array.

### Prefer Fols to Explicit Recursion

Do not try to write all function using `tail recursion` to benefit from `tail recursion optimization` (transforming the function in a while loop by the compiler).
Most time those functions can be written directly as a fold over an array or a similar data structure.
Using `map` and `fold` has the added adventage of code simplicity. They communicate the \_intent` of the algorithm much better.

#### Example: Reverse an array with foldr

**Remember** `<>` is the append operator

        > import Data.Foldable

        > :paste
        … reverse :: forall a. Array a -> Array a
        … reverse = foldr (\x xs -> xs <> [x]) []
        … ^D

        > reverse [1, 2, 3]
        [3,2,1]

**Explanation**: `foldr (\x xs -> xs <> [x]) []`

- `x xs -> xs <> [x]`: is a function that takes the next value `x` and the current accumulator `xs` and a new array. The array is starts with the current value, followed by the current accumulator. This generated array becomes the new accumulator.
- `[]`: is the initial value of the accumulator.

## A Virtual Filesystem

The `Data.Path` module defines an API for a virutal filesystem with the following definitions:

        root :: Path

        ls :: Path -> Array Path

        filename :: Path -> String

        size :: Path -> Maybe Int

        isDirectory :: Path -> Boolean

#### Example Listing All Files

**Remember:** `:` is the `cons` operator. Attaches an element to the front of an array, creating a new array.

This function performs a deep enumeration of all files inside a directory.
It has the following type

        > import Prelude
        > import Data.Path
        > import Data.Array ((:), concatMap)
        allFiles :: Path -> Array Path
        allFiles file = file : concatMap allFiles (ls file)

- `ls file`: Enumerate the immediate children of the directory.
- `allFile (ls file)`: For each sub-directory, call the `allFiles` function as well and return the generated list of files.
- `concatMap allFiles (ls files)` : `concatMap` applies the `allFiles` function and flattens the resulting list of files/sub-directories.
- `:`: is the `cons` operatore, which includes the current file to the generated list of `concatMap`.

**Explanation:**
This function is recursevely defined:

1. We list the contents of the current path
2. For each child (folder) within the current path, call this function again and append the result to our output.

We can create the same function useing `do notation`.
Recall that a backwards arrow corresponds to choosing an element from an array. The first step is to choose an element from the immediate children of the argument. Then we simply call the function recursively for that file. Since we are using do notation, **there is an implicit call to concatMap** which concatenates all of the recursive results.

#### Example: Listing All Files with Do Notation

        allFiles' :: Path -> Array Path
        allFiles' file = file : do
                child <- ls file
                allFiles' child
