module Test.MySolutions where

import Prelude

-- Note to reader: Add your solutions to this file
import Prelude
import Data.AddressBook (AddressBook, Entry)
import Data.List (filter, head, nubBy, null)
import Data.Maybe (Maybe)

findEntryByStreet :: String -> AddressBook -> Maybe Entry
findEntryByStreet streetName = head <<< filter filterEntry
  where
    filterEntry :: Entry -> Boolean
    filterEntry entry = entry.address.street == streetName

-- Type of Data.List.null
-- forall a. List a -> Boolean

-- Questin: Why is the parameter `book` important and cannot be removed via eta conversion?
-- Is it, because after filtering, the first and second `book` are different?
isInBook :: String -> String -> AddressBook -> Boolean
isInBook firstName lastName book = not null $ filter filterEntry book
    where
      filterEntry :: Entry -> Boolean
      filterEntry entry = entry.firstName == firstName && entry.lastName ==  lastName

-- Type of Data.List.nubBy
-- forall a. (a -> a -> Boolean) -> List a -> List a

{- 
    1. The first part is a function, that gets two elements of the type `a` 
    and returns a boolean, depending on a condition. In this case we want
    it to be true, if the firstName and lastName of two `Entry`s are the
    the same. Otherwise false
    2. The second part is, that `nubBy` accepts a List a as parameter. In 
    this case our addressbook.
    3. It returns a new List of type `a`, which is a new address book
    without any duplicates.
-}
isSameName :: Entry -> Entry -> Boolean
isSameName e1 e2 =  e1.firstName == e2.firstName &&
                    e1.lastName == e2.lastName

removeDuplicates :: AddressBook -> AddressBook
removeDuplicates = nubBy isSameName

