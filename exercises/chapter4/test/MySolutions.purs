module Test.MySolutions where

import Prelude
import Data.Path (Path, ls, root, filename, size, isDirectory)
import Data.Array (null, tail, head, filter, length, (..), (:))
import Data.Maybe (fromMaybe, maybe)
import Control.MonadZero (guard)
import Data.Foldable (foldl, foldr)

-- Note to reader: Add your solutions to this file

isEven :: Int -> Boolean
isEven n =
    if mod n 2 == 0
    then true
    else  false

{-
    maybe
    ---
    maybe :: forall a b. b -> (a -> b) -> Maybe a -> b
    Takes a default value, a function, and a Maybe value. 
    If the Maybe value is Nothing the default value is returned, 
    otherwise the function is applied to the value inside the Just and the 
    result is returned.

    head
    ---
    head :: forall a. Array a -> Maybe a
    Get the first element in an array, or Nothing if the array is empty
-}

{-
    We need another helper function that returns 1 if an Int is even,
    and 0 if it is not.
-}

oneIfEven :: Int -> Int
oneIfEven n = if isEven n then 1 else 0

{- 
    We need an accumulator to store the number of even values up to 
    this point. Thus we need an auxiliary function.
    This auxiliary function takes the original input array of Ints and
    a starting accumulator value (the number of even Ints encounted up to this point) 
    and returns the number of even Ints within the input array added to the accumulator.
-}

{-
    1. head rest: Returns the first element in `rest` array if it exists. Otherwise returns `Nothing`.
    2. maybe 0 oneIfEven $ head rest: If `head rest` is `Nothing`, return 0, else return the result of oneIfEven $ head rest.
    3. add acc $ (maybe...): Add the result of the previous line the input paramter value of acc.
    4. fromMaybe [] (tail rest): Returns the rest array without the first element. If the `rest` array has no  tail, create the empty array [].
    5. counteEvenAux (...): Recursively call `countEvenAux` again with the new array from line (4)
        and the new accumulator value from line (3). 
-}
countEvenAux :: Array Int -> Int -> Int
countEvenAux [] acc = acc {- in case of an empty array, return the accumalted count up to this point -}
countEvenAux rest acc  = countEvenAux (fromMaybe [] (tail rest)) $ add acc $  maybe 0 oneIfEven $ head rest

countEven :: Array Int -> Int
countEven  arr = countEvenAux arr 0


squared :: Array Number -> Array Number
squared arr = (\n -> n * n) `map` arr


keepNonNegative :: Array Number -> Array Number
keepNonNegative arr = filter (\n -> n >= 0.0 ) arr 


{-
Define an infix synonym <$?> for filter.
Write a keepNonNegativeRewrite function, which is the same as keepNonNegative, but replaces filter with your new infix operator <$?>.
Experiment with the precedence level and associativity of your operator in PSCi. NOTE: no unit tests for this.

Note: precendence level 0, 4, 8 are all ok.
-}

infix 8 filter as <$?>
keepNonNegativeRewrite :: Array Number -> Array Number
keepNonNegativeRewrite arr = (\n -> n >= 0.0) <$?> arr


{- For a number n, get the list of all factors [i, j] such that i*j = n -}
getFactors :: Int -> Array (Array Int)
getFactors n = do
    i <- 1 .. n
    j <- i .. n
    guard $ i * j == n
    pure [i, j]

isPrime :: Int -> Boolean
isPrime n = do
    let l = length (getFactors n)
    if n > 1 && l == 1
    then true
    else false

cartesianProduct :: forall a. Array a -> Array a -> Array (Array a)
cartesianProduct a b = do
    left_element <- a
    right_element <- b
    pure [left_element, right_element]

triples :: Int -> Array (Array Int)
triples n = do
    a <- 1 .. n
    b <- a .. n
    c <- b .. n
    guard $ a * a + b * b == c * c
    pure [ a, b, c]

{- TODO: factorize -}
{- 
factorize :: Int -> Array Int
factorize n = do
    let pFactors = []
    i <- 2 .. n
    guard $ isPrime i && n mod i == 0 &&   
-}

{-
    Explanation:
    - conj is just the real name of && (and) operator. It takes two booleans and returns whether both are true or not.
    signature: forall a. HeytingAlgebra a => a -> a -> a

    - foldl has the signature:  
    forall a b f. Foldable f => (b -> a -> b) -> b -> f a -> b
    Its first parameter is a function: it takes an acc, a new value a and applies the new value to the acc, creating a new acc.
    We use the `conj` for that. If all values have been true so far, comparing the current acc with a true value returns in a true value.
    Otherwise if the current acc is fals, the current value is false, or both are false, return false.
-}
allTrue :: Array Boolean -> Boolean
allTrue = foldl conj true

{-
    (Medium - No Test) Characterize those arrays xs for which the function 
    foldl (==) false xs returns true. 
    In other words, complete the sentence: 
    "The function returns true when xs contains ..."
        ...an uneven number of `false` elements..
-}

{-
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

-}


{- 
    Explanation from https://tgdwyer.github.io/purescript/
    We defina a helper function `aux` whose first parameter is an iteration counter.
    `aux 0 _ b = b`: Defines the base case for `aux`. So when the iteration counter is 0, return the third parameter `b`.

-}
fibTailRec :: Int -> Int
fibTailRec n = aux n 0 1
    where
      aux 0 _ b = b
      aux i a b = aux (i - 1) b (a + b)


reverse :: Array Int -> Array Int
reverse = foldl (\xs x -> [x] <> xs) []

{-
    Exercise:
    (Easy) Write a function onlyFiles which returns all files (not directories) in 
    all subdirectories of a directory.
-}

getAllFiles :: Path -> Array Path
getAllFiles file = file : do
  child <- ls file
  getAllFiles child
  
onlyFiles :: Path -> Array Path
onlyFiles path = filter(\el -> not isDirectory (el)) $ getAllFiles path
